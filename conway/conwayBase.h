#pragma once

#include <list>

class ConwayBase
{
public:
	ConwayBase(std::list<std::pair<long long, long long>> inputs) {};
	virtual ~ConwayBase() {};
	virtual void cellActivate(long long x, long long y) = 0;
	virtual void cellKill(long long x, long long y) = 0;
	virtual void update() = 0;
	virtual void getAreaCells(bool *area, long long x, long long y, unsigned int width, unsigned int height) = 0;
	virtual void reset() = 0;
};