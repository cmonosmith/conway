#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <fstream>
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

#include "conway2.h"
#include "conway3.h"

void runConwayVersion2();
void runConwayVersion3();
void updatePixelArray(sf::Uint8*, bool*, unsigned int);
void clearArea(bool*, unsigned int);

int main()
{
	// read settings from some json
	// stuff like the input file
	FILE* pFile;
	errno_t err = fopen_s(&pFile, "settings.json", "rb");
	char buffer[1024];
	rapidjson::FileReadStream is(pFile, buffer, sizeof(buffer));
	rapidjson::Document document;
	document.ParseStream<0, rapidjson::UTF8<>, rapidjson::FileReadStream>(is);

	int version = document["version"].GetInt();
	if (version == 2)
	{
		runConwayVersion2();
	}
	else if (version == 3)
	{
		runConwayVersion3();
	}
	return 0;
}

void runConwayVersion2()
{
	// create the conway thing
	Conway2 conway("version2inputs.txt", 1);

	// setup display parameters, and the delay between conway 'ticks'
	unsigned int width = 1280;
	unsigned int height = 720;
	sf::Int32 msBetweenUpdate = 128;

	// create a SFML window that is not resizeable
	sf::RenderWindow window(sf::VideoMode(width, height), "Monosmith's Conway's Game of Life", sf::Style::Titlebar | sf::Style::Close);
	window.setFramerateLimit(60);

	// init some states
	bool firstPass = true; // render on the first pass
	bool paused = false; // used to pause the conway rules, and allow drawing cells with the mouse

	// create the pixel array, the texture that uses copies of it, and the sprite that points at the texture
	sf::Uint8* pixels = new sf::Uint8[width * height * 4]; // * 4 because pixels have 4 components (ARGB)
	sf::Texture texture;
	if (!texture.create(width, height))
	{
		window.close();
	}
	sf::Sprite sprite;
	sprite.setTexture(texture);

	// set alpha to max, no transparency, through the whole pixel array
	for (unsigned int x = 0; x < width; x++)
	{
		for (unsigned int y = 0; y < height; y++)
		{
			unsigned int offset = (y * width * 4) + (x * 4);
			pixels[offset] = 255;
		}
	}

	sf::Clock clock;
	sf::Int32 sinceLastUpdate = 0;

	// run the program as long as the window is open
	while (window.isOpen())
	{
		sf::Int32 delta = clock.restart().asMilliseconds();
		sinceLastUpdate += delta;
		bool update = false;

		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (window.hasFocus())
				{
					switch (event.key.code)
					{
					case sf::Keyboard::Escape:
						conway.reset();
						paused = true;
						update = true;
						break;
					case sf::Keyboard::Add:
						conway.zoomIn();
						update = true;
						break;
					case sf::Keyboard::Subtract:
						conway.zoomOut();
						update = true;
						break;
					case sf::Keyboard::W:
					case sf::Keyboard::Up:
						conway.moveUp(height);
						update = true;
						break;
					case sf::Keyboard::A:
					case sf::Keyboard::Left:
						conway.moveLeft(width);
						update = true;
						break;
					case sf::Keyboard::S:
					case sf::Keyboard::Down:
						conway.moveDown(height);
						update = true;
						break;
					case sf::Keyboard::D:
					case sf::Keyboard::Right:
						conway.moveRight(width);
						update = true;
						break;
					case sf::Keyboard::Space:
					case sf::Keyboard::F6:
						paused = !paused;
						break;
					case sf::Keyboard::F7:
						// things get sloppy if the updates can't keep up with the target rate.
						// 8 and 16 get all stuttery when the board gets populated with a few hundred thousand cells, i think
						if (msBetweenUpdate > 8)
						{
							msBetweenUpdate >>= 1;
						}
						break;
					case sf::Keyboard::F5:
						if (msBetweenUpdate < 4096)
						{
							msBetweenUpdate <<= 1;
						}
						break;
					default:
						break;
					}
				}
				break;
			case sf::Event::MouseButtonPressed:
				if (window.hasFocus() && paused)
				{
					switch (event.mouseButton.button)
					{
					case sf::Mouse::Left:
						conway.cellOn(event.mouseButton.x, event.mouseButton.y);
						update = true;
						break;
					case sf::Mouse::Right:
						conway.cellOff(event.mouseButton.x, event.mouseButton.y);
						update = true;
						break;
					default:
						break;
					}
				}
			default:
				break;
			}
		}

		// update simulation every msBetweenUpdate ms
		if (sinceLastUpdate > msBetweenUpdate)
		{
			if (!paused)
			{
				conway.update();
				update = true;
			}
			// except no more than once per frame
			sinceLastUpdate = sinceLastUpdate % msBetweenUpdate;
		}

		// update a texture from an array of pixels
		if (update || firstPass)
		{
			firstPass = false;

			// zero out everything except alpha
			for (unsigned int x = 0; x < width; x++)
			{
				for (unsigned int y = 0; y < height; y++)
				{
					unsigned int offset = (y * width * 4) + (x * 4);
					for (unsigned int rgb = 1; rgb < 4; rgb++)
					{
						pixels[offset + rgb] = 0;
					}
				}
			}

			conway.updatePixels(pixels, width, height);
			texture.update(pixels);
		}

		// clear the window with black color
		window.clear(sf::Color::Black);

		// draw the sprite
		window.draw(sprite);

		// end the current frame
		window.display();
	}

	delete pixels;
}

void runConwayVersion3()
{
	// replace input file with that special format

	//init board, read live cells from input file
	std::ifstream inputFile;
	inputFile.open("version3inputs.txt", std::ios::in);
	long long x, y;
	std::list<std::pair<long long, long long>> inputs;
	while (inputFile >> x >> y)
	{
		inputs.push_back(std::make_pair(x, y));
	}
	Conway3 conway(inputs);


	// setup display parameters, and the delay between conway 'ticks'
	unsigned int width = 512;
	unsigned int height = 512;
	unsigned int size = width * height;
	sf::Int32 usBetweenUpdate = 128000;

	// start in the center of our universe
	long long viewX = 0;
	long long viewY = 0;
	unsigned int gridPerPixel = 1;
	unsigned int pixelPerGrid = 1; // start off a little zoomed in

	// create a SFML window that is not resizeable
	sf::RenderWindow window(sf::VideoMode(width, height), "Monosmith's Conway's Game of Life", sf::Style::Titlebar | sf::Style::Close);
	window.setFramerateLimit(0);

	// init some state
	bool paused = false; // used to pause the conway rules, and allow drawing cells with the mouse

	sf::Clock clock;
	sf::Int32 sinceLastUpdate = 0;
	sf::Int32 sinceLastDraw = 0;
	sf::Int32 timeUpdating = 0;
	int updates = 0;
	sf::Int32 timeDrawing = 0;
	int draws = 0;

	// run the program as long as the window is open
	while (window.isOpen())
	{
		sf::Int32 delta = clock.restart().asMicroseconds();
		sinceLastUpdate += delta;
		sinceLastDraw += delta;

		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (window.hasFocus())
				{
					switch (event.key.code)
					{
					case sf::Keyboard::Escape:
						conway.reset();
						paused = true;
						break;
					case sf::Keyboard::Add:
						if (pixelPerGrid < 8)
						{
							pixelPerGrid <<= 1;
						}
						break;
					case sf::Keyboard::Subtract:
						if (pixelPerGrid > 1)
						{
							pixelPerGrid >>= 1;
						}
						break;
					case sf::Keyboard::W:
					case sf::Keyboard::Up:
						viewY -= height / pixelPerGrid / 2;
						break;
					case sf::Keyboard::A:
					case sf::Keyboard::Left:
						viewX -= width / pixelPerGrid / 2;
						break;
					case sf::Keyboard::S:
					case sf::Keyboard::Down:
						viewY += height / pixelPerGrid / 2;
						break;
					case sf::Keyboard::D:
					case sf::Keyboard::Right:
						viewX += width / pixelPerGrid / 2;
						break;
					case sf::Keyboard::Space:
					case sf::Keyboard::F6:
						paused = !paused;
						break;
					case sf::Keyboard::F7:
						if (usBetweenUpdate > 1000)
						{
							usBetweenUpdate >>= 1;
						}
						break;
					case sf::Keyboard::F5:
						if (usBetweenUpdate < 4096000)
						{
							usBetweenUpdate <<= 1;
						}
						break;
					default:
						break;
					}
				}
				break;
			case sf::Event::MouseButtonPressed:
				if (window.hasFocus() && paused)
				{
					switch (event.mouseButton.button)
					{
					case sf::Mouse::Left:
						conway.cellActivate(viewX + (event.mouseButton.x / pixelPerGrid), viewY + (event.mouseButton.y / pixelPerGrid));
						break;
					case sf::Mouse::Right:
						conway.cellKill(viewX + (event.mouseButton.x / pixelPerGrid), viewY + (event.mouseButton.y / pixelPerGrid));
						break;
					default:
						break;
					}
				}
			default:
				break;
			}
		}

		// update simulation every msBetweenUpdate ms
		if (sinceLastUpdate > usBetweenUpdate)
		{
			if (!paused)
			{
				sf::Clock updateTime;
				conway.update();
				timeUpdating += updateTime.restart().asMicroseconds();
				updates++;
			}
			// except no more than once per loop
			sinceLastUpdate = sinceLastUpdate % usBetweenUpdate;
		}

		// try to draw up to 60 times per second (no sooner than 16666us since last draw)
		if (sinceLastDraw > 16666)
		{
			// draw living cells
			sf::Clock drawingTime;
			std::list<std::pair<sf::Sprite *, sf::Texture *>> sprites;
			try
			{
				sprites = conway.drawAreaCells(viewX, viewY, width, height, gridPerPixel, pixelPerGrid);
			}
			catch (int n)
			{
				window.close();
				break;
			}
			// clear window to black
			window.clear(sf::Color::Black);
			// draw sprites
			for (std::list<std::pair<sf::Sprite *, sf::Texture *>>::iterator it = sprites.begin(); it != sprites.end(); ++it)
			{
				window.draw(*it->first);
			}
			// end frame
			window.display();
			// destroy
			for (std::list<std::pair<sf::Sprite *, sf::Texture *>>::iterator it = sprites.begin(); it != sprites.end(); ++it)
			{
				delete it->first;
				delete it->second;
			}
			timeDrawing += drawingTime.restart().asMicroseconds();
			draws++;
			// reset frame timer
			sinceLastDraw = 0;
		}
	}
}

void updatePixelArray(sf::Uint8 *pixels, bool *cells, unsigned int size)
{
	for (unsigned int index = 0; index < size; index++)
	{
		if (cells[index])
		{
			for (unsigned int rgb = 1; rgb < 4; rgb++)
			{
				pixels[(index << 2) + rgb] = 255;
			}
		}
	}
}

void clearArea(bool *cells, unsigned int size)
{
	for (unsigned int index = 0; index < size; index++)
	{
		cells[index] = false;
	}
}