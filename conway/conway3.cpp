#include "conway3.h"

// activate cells specified in the input list
Conway3::Conway3(std::list<std::pair<long long, long long>> inputs)
	// : ConwayBase(inputs)
{
	for (std::list<std::pair<long long, long long>>::iterator it = inputs.begin(); it != inputs.end(); ++it)
	{
		cellActivate(it->first, it->second);
	}
	for (unsigned char i = 0; ; i++)
	{
		bitCount[i] = countBits(i);
		getByte(i, bytes[i]);
		if (i == 255)
		{
			break;
		}
	}
}

// wreck the board, free memory, whatever
Conway3::~Conway3()
{
	reset();
}

unsigned char Conway3::countBits(unsigned char i)
{
	unsigned char count = 0;
	for (unsigned char bit = 0; bit < 8; bit++)
	{
		if (i & (0x01 << bit))
		{
			count++;
		}
	}
	return count;
}

void Conway3::getByte(unsigned char i, sf::Uint8 byte[32])
{
	unsigned char offset;
	for (unsigned char bit = 0; bit < 8; bit++)
	{
		offset = (7 - bit) << 2;
		byte[offset] = 255;
		if (i & (0x01 << bit))
		{
			byte[offset + 1] = 255;
			byte[offset + 2] = 255;
			byte[offset + 3] = 255;
		}
		else
		{
			byte[offset + 1] = 0;
			byte[offset + 2] = 0;
			byte[offset + 3] = 0;
		}
	}
}

// turn on a single cell at a given location in 64-bit space
void Conway3::cellActivate(long long x, long long y)
{
	std::pair<long long, long long> coords = std::make_pair(x >> 5, y >> 5);
	Block *block;
	if (board.count(coords) > 0)
	{
		block = board[coords];
	}
	else
	{
		block = new Block(true);
		board[coords] = block;
		addBlocksToNeighbors(coords, block);
	}
	block->rows[y & 0x0000001f] |= 0x00000001 << (31 - (x & 0x0000001f));
	block->active = true;
}

// turn off a cell at a given location in 64-bit space
void Conway3::cellKill(long long x, long long y)
{
	std::pair<long long, long long> coords = std::make_pair(x >> 5, y >> 5);
	if (board.count(coords) > 0)
	{
		Block *block = board[coords];
		block->rows[y & 0x0000001f] &= ~(0x00000001 << (31 - (x & 0x0000001f)));
		block->active = true;
	}
}

// move forward in 64-bit spacetime one "tick" by Conway's rules
void Conway3::update()
{
	Block *block;
	std::pair<long long, long long> coords;
	bool wasActive;
	std::unordered_map<std::pair<long long, long long>, Block*, PairHasher>::iterator it;
	// perform the update steps for each block
	for (it = board.begin(); it != board.end(); ++it)
	{
		// idea: could check if it's active, but also when any border cell changes, poke neighbors
		// if a neighbor gets poked, just check the border cells
		block = it->second;
		coords = it->first;
		wasActive = block->active;
		//block->active = false;
		updateTopRow(block, coords.first, coords.second);
		updateCentralRows(block, coords.first, coords.second, wasActive);
		updateBottomRow(block, coords.first, coords.second);
	}
	// iterate over newCells, keep in mind that they're all 0s, so don't bother with some of the checks
	for (it = newBlocks.begin(); it != newBlocks.end(); ++it)
	{
		block = it->second;
		coords = it->first;
		//block->active = false;
		updateTopRow(block, coords.first, coords.second);
		updateCentralRows(block, coords.first, coords.second, false);
		updateBottomRow(block, coords.first, coords.second);
		board[coords] = block;
	}
	newBlocks.clear();

	// copy the updated block states to the main block states
	for (it = board.begin(); it != board.end(); ++it)
	{
		block = it->second;
		if (block->active)
		{
			// could replace with memcpy 32*4 bytes and memset...
			for (int i = 0; i < 32; i++)
			{
				block->rows[i] = block->next[i];
				//block->next[i] = 0;
			}
		}
	}
}

// update the top row. return true if anything changed (block is active)
void Conway3::updateTopRow(Block *block, long long x, long long y)
{
	updateTopLeft(block, x, y);
	for (int bit = 0; bit < 30; bit++) {
		updateTopInner(block, bit, x, y);
	}
	updateTopRight(block, x, y);
}

// update top left cell. return true if it changed (block is active)
void Conway3::updateTopLeft(Block *block, long long x, long long y)
{
	// form a byte from the surrounding 8 bits (cells)
	unsigned char neighboringBits = 0;
	if (block->neighbors[0] != nullptr)
	{
		neighboringBits |= (block->neighbors[0]->rows[31] & 0x00000001) << 7;
	}
	if (block->neighbors[1] != nullptr)
	{
		neighboringBits |= (block->neighbors[1]->rows[31] & 0xC0000000) >> 25;
	}
	if (block->neighbors[3] != nullptr)
	{
		neighboringBits |= (block->neighbors[3]->rows[0] & 0x00000001) << 4;
		neighboringBits |= (block->neighbors[3]->rows[1] & 0x00000001) << 2;
	}
	neighboringBits |= (block->rows[0] & 0x40000000) >> 27;
	neighboringBits |= (block->rows[1] & 0xC0000000) >> 30;

	unsigned char count = bitCount[neighboringBits];
	bool alive = block->rows[0] & 0x80000000;

	// determine if the cell changes state, and thus if the block is active
	if (alive)
	{
		if (count == 2 || count == 3)
		{
			block->next[0] |= 0x80000000;
		}
		else
		{
			block->next[0] &= 0x7fffffff;
			block->active = true;
		}
	}
	else
	{
		if (count == 3)
		{
			block->next[0] |= 0x80000000;
			block->active = true;
		}
	}
	
	// if the upper left bit in this block is 1, we care to check adjacent blocks
	if (alive)
	{
		// create adjacent blocks if they don't exist. set active true so they're checked once.
		if (block->neighbors[0] == nullptr)
		{
			Block *upLeft = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x - 1, y - 1);
			newBlocks[coords] = upLeft;
			block->neighbors[0] = upLeft;
			upLeft->neighbors[7] = block;
			addBlocksToNeighbors(coords, upLeft);
		}
		if (block->neighbors[1] == nullptr)
		{
			Block *up = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x, y - 1);
			newBlocks[coords] = up;
			block->neighbors[1] = up;
			up->neighbors[6] = block;
			addBlocksToNeighbors(coords, up);
		}
		if (block->neighbors[3] == nullptr)
		{
			Block *left = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x - 1, y);
			newBlocks[coords] = left;
			block->neighbors[3] = left;
			left->neighbors[4] = block;
			addBlocksToNeighbors(coords, left);
		}
	}
}

// update the inner portion of the top row, returning true if anything changed
void Conway3::updateTopInner(Block *block, unsigned char bit, long long x, long long y)
{
	// form a byte from the surrounding 8 bits (cells)
	unsigned char neighboringBits = 0;
	unsigned long bits;
	if (block->neighbors[1] != nullptr)
	{
		bits = (block->neighbors[1]->rows[31] & (0x00000007 << bit)) >> bit;
		neighboringBits |= bits << 5;
	}
	bits = (block->rows[0] & (0x00000004 << bit)) >> bit;
	neighboringBits |= bits << 2;
	bits = (block->rows[0] & (0x00000001 << bit)) >> bit;
	neighboringBits |= bits << 3;
	neighboringBits |= (block->rows[1] & (0x00000007 << bit)) >> bit;

	unsigned char count = bitCount[neighboringBits];
	bool alive = block->rows[0] & 0x00000002 << bit;

	// determine if the cell changes state, and thus if the block is active
	if (alive)
	{
		if (count == 2 || count == 3)
		{
			block->next[0] |= 0x00000002 << bit;
		}
		else
		{
			block->next[0] &= ~(0x00000002 << bit);
			block->active = true;
		}
	}
	else
	{
		if (count == 3)
		{
			block->next[0] |= 0x00000002 << bit;
			block->active = true;
		}
	}

	// if the upper bit in this block is 1, we care to check adjacent blocks
	if (alive)
	{
		// create adjacent blocks if they don't exist. set active true so they're checked once.
		if (block->neighbors[1] == nullptr)
		{
			Block *up = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x, y - 1);
			newBlocks[coords] = up;
			block->neighbors[1] = up;
			up->neighbors[6] = block;
			addBlocksToNeighbors(coords, up);
		}
	}
}

// update the top right cell. return true if it changed (block is active)
void Conway3::updateTopRight(Block *block, long long x, long long y)
{
	// form a byte from the surrounding 8 bits (cells)
	unsigned char neighboringBits = 0;
	if (block->neighbors[1] != nullptr)
	{
		neighboringBits |= (block->neighbors[1]->rows[31] & 0x00000003) << 6;
	}
	if (block->neighbors[2] != nullptr)
	{
		neighboringBits |= (block->neighbors[2]->rows[31] & 0x80000000) >> 26;
	}
	if (block->neighbors[4] != nullptr)
	{
		neighboringBits |= (block->neighbors[4]->rows[0] & 0x80000000) >> 28;
		neighboringBits |= (block->neighbors[4]->rows[1] & 0x80000000) >> 31;
	}
	neighboringBits |= (block->rows[0] & 0x00000002) << 3;
	neighboringBits |= (block->rows[1] & 0x00000003) << 1;

	unsigned char count = bitCount[neighboringBits];
	bool alive = block->rows[0] & 0x00000001;

	// determine if the cell changes state, and thus if the block is active
	if (alive)
	{
		if (count == 2 || count == 3)
		{
			block->next[0] |= 0x00000001;
		}
		else
		{
			block->next[0] &= 0xfffffffe;
			block->active = true;
		}
	}
	else
	{
		if (count == 3)
		{
			block->next[0] |= 0x00000001;
			block->active = true;
		}
	}

	// if the upper right bit in this block is 1, we care to check adjacent blocks
	if (alive)
	{
		// create adjacent blocks if they don't exist. set active true so they're checked once.
		if (block->neighbors[1] == nullptr)
		{
			Block *up = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x, y - 1);
			newBlocks[coords] = up;
			block->neighbors[1] = up;
			up->neighbors[6] = block;
			addBlocksToNeighbors(coords, up);
		}
		if (block->neighbors[2] == nullptr)
		{
			Block *upRight = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x + 1, y - 1);
			newBlocks[coords] = upRight;
			block->neighbors[2] = upRight;
			upRight->neighbors[5] = block;
			addBlocksToNeighbors(coords, upRight);
		}
		if (block->neighbors[4] == nullptr)
		{
			Block *right = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x + 1, y);
			newBlocks[coords] = right;
			block->neighbors[4] = right;
			right->neighbors[3] = block;
			addBlocksToNeighbors(coords, right);
		}
	}
}

// update the 30 central rows, returning true if anything changed
void Conway3::updateCentralRows(Block *block, long long x, long long y, bool includeInner)
{
	for (unsigned char row = 1; row < 31; row++)
	{
		updateCentralLeft(block, row, x, y);
		if (includeInner)
		{
			for (unsigned char bit = 0; bit < 30; bit++)
			{
				updateCentralInner(block, row, bit, x, y);
			}
		}
		updateCentralRight(block, row, x, y);
	}
}

// update the left-most cell of a central row, returning true if it changed
void Conway3::updateCentralLeft(Block *block, unsigned char row, long long x, long long y)
{
	// form a byte from the surrounding 8 bits (cells)
	unsigned char neighboringBits = 0;
	if (block->neighbors[3] != nullptr)
	{
		neighboringBits |= (block->neighbors[3]->rows[row - 1] & 0x00000001) << 7;
		neighboringBits |= (block->neighbors[3]->rows[row] & 0x00000001) << 4;
		neighboringBits |= (block->neighbors[3]->rows[row + 1] & 0x00000001) << 2;
	}
	neighboringBits |= (block->rows[row - 1] & 0xC0000000) >> 25;
	neighboringBits |= (block->rows[row] & 0x40000000) >> 27;
	neighboringBits |= (block->rows[row + 1] & 0xC0000000) >> 30;

	unsigned char count = bitCount[neighboringBits];
	bool alive = block->rows[row] & 0x80000000;

	// determine if the cell changes state, and thus if the block is active
	if (alive)
	{
		if (count == 2 || count == 3)
		{
			block->next[row] |= 0x80000000;
		}
		else
		{
			block->next[row] &= 0x7fffffff;
			block->active = true;
		}
	}
	else
	{
		if (count == 3)
		{
			block->next[row] |= 0x80000000;
			block->active = true;
		}
	}

	// if the left-most bit in this block is 1, we care to check adjacent blocks
	if (alive)
	{
		// create adjacent blocks if they don't exist. set active true so they're checked once.
		if (block->neighbors[3] == nullptr)
		{
			Block *left = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x - 1, y);
			newBlocks[coords] = left;
			block->neighbors[3] = left;
			left->neighbors[4] = block;
			addBlocksToNeighbors(coords, left);
		}
	}
}

// update the inner cells of a central row, returning true if any changed
void Conway3::updateCentralInner(Block *block, unsigned char row, unsigned char bit, long long x, long long y)
{
	// form a byte from the surrounding 8 bits (cells)
	unsigned char neighboringBits = 0;
	unsigned long bits;
	bits = (block->rows[row - 1] & (0x00000007 << bit)) >> bit;
	neighboringBits |= bits << 5;
	bits = (block->rows[row] & (0x00000004 << bit)) >> bit;
	neighboringBits |= bits << 2;
	bits = (block->rows[row] & (0x00000001 << bit)) >> bit;
	neighboringBits |= bits << 3;
	neighboringBits |= (block->rows[row + 1]  & (0x00000007 << bit)) >> bit;

	unsigned char count = bitCount[neighboringBits];
	bool alive = block->rows[row] & 0x00000002 << bit;

	// determine if the cell changes state, and thus if the block is active
	if (alive)
	{
		if (count == 2 || count == 3)
		{
			block->next[row] |= 0x00000002 << bit;
		}
		else
		{
			block->next[row] &= ~(0x00000002 << bit);
			block->active = true;
		}
	}
	else
	{
		if (count == 3)
		{
			block->next[row] |= 0x00000002 << bit;
			block->active = true;
		}
	}
}

// update the right-most cell of a central row, returning true if it changed
void Conway3::updateCentralRight(Block *block, unsigned char row, long long x, long long y)
{
	// form a byte from the surrounding 8 bits (cells)
	unsigned char neighboringBits = 0;
	neighboringBits |= (block->rows[row - 1] & 0x00000003) << 6;
	neighboringBits |= (block->rows[row] & 0x00000002) << 3;
	neighboringBits |= (block->rows[row + 1] & 0x00000003) << 1;
	if (block->neighbors[4] != nullptr)
	{
		neighboringBits |= (block->neighbors[4]->rows[row - 1] & 0x80000000) >> 26;
		neighboringBits |= (block->neighbors[4]->rows[row] & 0x80000000) >> 28;
		neighboringBits |= (block->neighbors[4]->rows[row + 1] & 0x80000000) >> 31;
	}

	unsigned char count = bitCount[neighboringBits];
	bool alive = block->rows[row] & 0x00000001;

	// determine if the cell changes state, and thus if the block is active
	if (alive)
	{
		if (count == 2 || count == 3)
		{
			block->next[row] |= 0x00000001;
		}
		else
		{
			block->next[row] &= 0xfffffffe;
			block->active = true;
		}
	}
	else
	{
		if (count == 3)
		{
			block->next[row] |= 0x00000001;
			block->active = true;
		}
	}

	// if the left-most bit in this block is 1, we care to check adjacent blocks
	if (alive)
	{
		// create adjacent blocks if they don't exist. set active true so they're checked once.
		if (block->neighbors[4] == nullptr)
		{
			Block *right = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x + 1, y);
			newBlocks[coords] = right;
			block->neighbors[4] = right;
			right->neighbors[3] = block;
			addBlocksToNeighbors(coords, right);
		}
	}
}

// update the bottom row. return true if anything changed (block is active)
void Conway3::updateBottomRow(Block *block, long long x, long long y)
{
	updateBottomLeft(block, x, y);
	for (int bit = 0; bit < 30; bit++) {
		updateBottomInner(block, bit, x, y);
	}
	updateBottomRight(block, x, y);
}

// update bottom left cell. return true if it changed (block is active)
void Conway3::updateBottomLeft(Block *block, long long x, long long y)
{
	// form a byte from the surrounding 8 bits (cells)
	unsigned char neighboringBits = 0;
	if (block->neighbors[3] != nullptr)
	{
		neighboringBits |= (block->neighbors[3]->rows[30] & 0x00000001) << 7;
		neighboringBits |= (block->neighbors[3]->rows[31] & 0x00000001) << 4;
	}
	neighboringBits |= (block->rows[30] & 0xC0000000) >> 25;
	neighboringBits |= (block->rows[31] & 0x40000000) >> 27;
	if (block->neighbors[5] != nullptr)
	{
		neighboringBits |= (block->neighbors[5]->rows[0] & 0x00000001) << 2;
	}
	if (block->neighbors[6] != nullptr)
	{
		neighboringBits |= (block->neighbors[6]->rows[0] & 0xC0000000) >> 30;
	}

	unsigned char count = bitCount[neighboringBits];
	bool alive = block->rows[31] & 0x80000000;

	// determine if the cell changes state, and thus if the block is active
	if (alive)
	{
		if (count == 2 || count == 3)
		{
			block->next[31] |= 0x80000000;
		}
		else
		{
			block->next[31] &= 0x7fffffff;
			block->active = true;
		}
	}
	else
	{
		if (count == 3)
		{
			block->next[31] |= 0x80000000;
			block->active = true;
		}
	}

	// if the bottom left bit in this block is 1, we care to check adjacent blocks
	if (alive)
	{
		// create adjacent blocks if they don't exist. set active true so they're checked once.
		if (block->neighbors[3] == nullptr)
		{
			Block *left = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x - 1, y);
			newBlocks[coords] = left;
			block->neighbors[3] = left;
			left->neighbors[4] = block;
			addBlocksToNeighbors(coords, left);
		}
		if (block->neighbors[5] == nullptr)
		{
			Block *downLeft = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x - 1, y + 1);
			newBlocks[coords] = downLeft;
			block->neighbors[5] = downLeft;
			downLeft->neighbors[2] = block;
			addBlocksToNeighbors(coords, downLeft);
		}
		if (block->neighbors[6] == nullptr)
		{
			Block *down = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x, y + 1);
			newBlocks[coords] = down;
			block->neighbors[6] = down;
			down->neighbors[1] = block;
			addBlocksToNeighbors(coords, down);
		}
	}
}

// update inner cells of the bottom row of the block, returning true if any changed
void Conway3::updateBottomInner(Block *block, unsigned char bit, long long x, long long y)
{
	// form a byte from the surrounding 8 bits (cells)
	unsigned char neighboringBits = 0;
	unsigned long bits;
	bits = (block->rows[30] & (0x00000007 << bit)) >> bit;
	neighboringBits |= bits << 5;
	bits = (block->rows[31] & (0x00000004 << bit)) >> bit;
	neighboringBits |= bits << 2;
	bits = (block->rows[31] & (0x00000001 << bit)) >> bit;
	neighboringBits |= bits << 3;
	if (block->neighbors[6] != nullptr)
	{
		neighboringBits |= (block->neighbors[6]->rows[0] & (0x00000007 << bit)) >> bit;
	}

	unsigned char count = bitCount[neighboringBits];
	bool alive = block->rows[31] & 0x00000002 << bit;

	// determine if the cell changes state, and thus if the block is active
	if (alive)
	{
		if (count == 2 || count == 3)
		{
			block->next[31] |= 0x00000002 << bit;
		}
		else
		{
			block->next[31] &= ~(0x00000002 << bit);
			block->active = true;
		}
	}
	else
	{
		if (count == 3)
		{
			block->next[31] |= 0x00000002 << bit;
			block->active = true;
		}
	}

	// if the bottom bit in this block is 1, we care to check adjacent blocks
	if (alive)
	{
		// create adjacent blocks if they don't exist. set active true so they're checked once.
		if (block->neighbors[6] == nullptr)
		{
			Block *down = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x, y + 1);
			newBlocks[coords] = down;
			block->neighbors[6] = down;
			down->neighbors[1] = block;
			addBlocksToNeighbors(coords, down);
		}
	}
}

// update the right-most cell of the bottom row, returning true if it changed
void Conway3::updateBottomRight(Block *block, long long x, long long y)
{
	// form a byte from the surrounding 8 bits (cells)
	unsigned char neighboringBits = 0;
	neighboringBits |= (block->rows[30] & 0x00000003) << 6;
	neighboringBits |= (block->rows[31] & 0x00000002) << 3;
	if (block->neighbors[4] != nullptr)
	{
		neighboringBits |= (block->neighbors[4]->rows[30] & 0x80000000) >> 26;
		neighboringBits |= (block->neighbors[4]->rows[31] & 0x80000000) >> 28;
	}
	if (block->neighbors[6] != nullptr)
	{
		neighboringBits |= (block->neighbors[6]->rows[0] & 0x00000003) << 1;
	}
	if (block->neighbors[7] != nullptr)
	{
		neighboringBits |= (block->neighbors[7]->rows[0] & 0x80000000) >> 31;
	}

	unsigned char count = bitCount[neighboringBits];
	bool alive = block->rows[31] & 0x00000001;

	// determine if the cell changes state, and thus if the block is active
	if (alive)
	{
		if (count == 2 || count == 3)
		{
			block->next[31] |= 0x00000001;
		}
		else
		{
			block->next[31] &= 0xfffffffe;
			block->active = true;
		}
	}
	else
	{
		if (count == 3)
		{
			block->next[31] |= 0x00000001;
			block->active = true;
		}
	}

	// if the bottom right bit in this block is 1, we care to check adjacent blocks
	if (alive)
	{
		// create adjacent blocks if they don't exist. set active true so they're checked once.
		if (block->neighbors[4] == nullptr)
		{
			Block *right = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x + 1, y);
			newBlocks[coords] = right;
			block->neighbors[4] = right;
			right->neighbors[3] = block;
			addBlocksToNeighbors(coords, right);
		}
		if (block->neighbors[6] == nullptr)
		{
			Block *down = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x, y + 1);
			newBlocks[coords] = down;
			block->neighbors[6] = down;
			down->neighbors[1] = block;
			addBlocksToNeighbors(coords, down);
		}
		if (block->neighbors[7] == nullptr)
		{
			Block *downRight = new Block(true);
			std::pair<long long, long long> coords = std::make_pair(x + 1, y + 1);
			newBlocks[coords] = downRight;
			block->neighbors[7] = downRight;
			downRight->neighbors[0] = block;
			addBlocksToNeighbors(coords, downRight);
		}
	}
}

// turn the board into an array of bools representing cells on(true) or off(false) in a given area
std::list<std::pair<sf::Sprite *, sf::Texture *>> Conway3::drawAreaCells(long long x, long long y, unsigned int width, unsigned int height, unsigned int gridPerPixel, unsigned int pixelPerGrid)
{
	std::pair<long long, long long> coords;
	Block *block;
	unsigned char *blockRow;
	std::list<std::pair<sf::Sprite *, sf::Texture *>> sprites;
	
	long long minX = x >> 5;
	long long maxX = (x + (width / pixelPerGrid) - 1) >> 5;
	long long minY = y >> 5;
	long long maxY = (y + (height / pixelPerGrid) - 1) >> 5;

	// create
	float screenY = 0;
	for (long long coordY = minY; coordY <= maxY; coordY++)
	{
		float screenX = 0;
		for (long long coordX = minX; coordX <= maxX; coordX++)
		{
			coords = std::make_pair(coordX, coordY);
			if (board.count(coords) > 0)
			{
				block = board[coords];
				for (int row = 0; row < 32; row++)
				{
					blockRow = (unsigned char *)(&(block->rows[row]));
					for (int byte = 0; byte < 4; byte++)
					{
						memcpy(pixels + (row * 32 * 4) + (byte * 8 * 4), bytes[blockRow[3 - byte]], 32); // little endian...?
					}
				}
				sf::Texture *texture = new sf::Texture;
				if (!texture->create(32, 32))
				{
					throw 100;
				}
				texture->update(pixels);
				texture->setSmooth(false);
				sf::Sprite *sprite = new sf::Sprite;
				sprite->setTexture(*texture);
				sprite->setPosition(sf::Vector2f(screenX, screenY));
				sprite->setScale(sf::Vector2f(pixelPerGrid, pixelPerGrid));
				sprites.push_back(std::make_pair(sprite, texture));
			}
			screenX += 32 * pixelPerGrid;
		}
		screenY += 32 * pixelPerGrid;
	}

	return sprites;
}

// iterate over the map of blocks and free memory, aka wreck
void Conway3::reset()
{
	for (std::unordered_map<std::pair<long long, long long>, Block*, PairHasher>::iterator it = board.begin(); it != board.end(); ++it)
	{
		delete it->second;
	}
	board.clear();
	for (std::unordered_map<std::pair<long long, long long>, Block*, PairHasher>::iterator it = newBlocks.begin(); it != newBlocks.end(); ++it)
	{
		delete it->second;
	}
	newBlocks.clear();
}

// call when a new block is created to assign neighboring blocks to each others' neighbors arrays
void Conway3::addBlocksToNeighbors(std::pair<long long, long long> coords, Block *block)
{
	// need to wrap, because integer overflow doesn't work any more (>>5)
	makeNewNeighbors(std::make_pair(coords.first - 1, coords.second - 1), block, 7, 0);
	makeNewNeighbors(std::make_pair(coords.first,     coords.second - 1), block, 6, 1);
	makeNewNeighbors(std::make_pair(coords.first + 1, coords.second - 1), block, 5, 2);
	makeNewNeighbors(std::make_pair(coords.first - 1, coords.second),     block, 4, 3);
	makeNewNeighbors(std::make_pair(coords.first + 1, coords.second),     block, 3, 4);
	makeNewNeighbors(std::make_pair(coords.first - 1, coords.second + 1), block, 2, 5);
	makeNewNeighbors(std::make_pair(coords.first,     coords.second + 1), block, 1, 6);
	makeNewNeighbors(std::make_pair(coords.first + 1, coords.second + 1), block, 0, 7);
}

// make a block neighbors with another block, specifying the respective indexes at which to assign each block
void Conway3::makeNewNeighbors(std::pair<long long, long long> coords, Block *block, int neighborIndex, int blockIndex)
{
	if (block->neighbors[blockIndex] == nullptr)
	{
		if (board.count(coords) > 0)
		{
			Block *neighbor = board[coords];
			neighbor->neighbors[neighborIndex] = block;
			block->neighbors[blockIndex] = neighbor;
		}
		else if (newBlocks.count(coords) > 0)
		{
			Block *neighbor = newBlocks[coords];
			neighbor->neighbors[neighborIndex] = block;
			block->neighbors[blockIndex] = neighbor;
		}
	}
}