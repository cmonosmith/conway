#include "conway2.h"

#include <iostream>
#include <fstream>
#include <list>

Conway2::Conway2(std::string inputFile, int curvature)
	: curvature(curvature != 0)
{
	//init board, read live cells from input file
	std::ifstream inputs;
	inputs.open(inputFile, std::ios::in);
	long long x, y;
	while (inputs >> x >> y)
	{
		Cell *cell = new Cell(true, true, false, false);
		board[y][x] = cell;
	}
	inputs.close();

	// start in the center of our universe
	viewX = 0;
	viewY = 0;
	gridPerPixel = 1;
	pixelPerGrid = 4; // start off a little zoomed in
}

// wreck all the allocated cells
Conway2::~Conway2()
{
	reset();
}

// update's the state of the universe by one 'tick'
void Conway2::update()
{
	// reset the visited status of all cells
	for (std::map<long long, std::map<long long, Cell*>>::iterator it = board.begin(); it != board.end(); ++it)
	{
		for (std::map<long long, Cell*>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
		{
			it2->second->visited = false;
		}
	}

	// visit everything, keeping track of cells that are changing in a new list
	std::list<Cell*> changingCells;
	for (std::map<long long, std::map<long long, Cell*>>::iterator it = board.begin(); it != board.end(); ++it)
	{
		for (std::map<long long, Cell*>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
		{
			Cell *cell = it2->second;
			checkCell(cell, it2->first, it->first, true);
			if (cell->willChange)
			{
				changingCells.push_back(cell);
			}
		}
	}

	// change everything that needs changing
	for (std::list<Cell*>::iterator it = changingCells.begin(); it != changingCells.end(); ++it)
	{
		(*it)->alive = !(*it)->alive;
		(*it)->willChange = false;
		(*it)->real = true;
	}
	changingCells.clear();
	
	// also copy all newly created neighbors, whether alive or not, to the main board
	for (std::map<long long, std::map<long long, Cell*>>::iterator it = newCells.begin(); it != newCells.end(); ++it)
	{
		for (std::map<long long, Cell*>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
		{
			board[it->first][it2->first] = it2->second;
			if (it2->second->willChange)
			{
				it2->second->real = true;
				it2->second->alive = true;
				it2->second->willChange = false;
			}
		}
		it->second.clear();
	}
	newCells.clear();
}

// visit a given cell, counting living neighbors to see if it changes
void Conway2::checkCell(Cell *cell, long long x, long long y, bool checkNewNeighbors)
{
	if (!cell->visited)
	{
		cell->visited = true;
		int livingNeighbors = 0;
		// checking cells above and below
		for (int yDelta = -1; yDelta <= 1 && (checkNewNeighbors || livingNeighbors <= 4); yDelta++)
		{
			if (!curvature) {
				if ((y == _I64_MIN && yDelta < 0)
					|| (y == _I64_MAX && yDelta > 0)) {
					continue;
				}
			}
			// checking cells left and right
			for (int xDelta = -1; xDelta <= 1 && (checkNewNeighbors || livingNeighbors <= 4); xDelta++)
			{
				// skip self
				if (yDelta == 0 && xDelta == 0)
				{
					continue;
				}
				if (!curvature) {
					if ((x == _I64_MIN && xDelta < 0)
						|| (x == _I64_MAX && xDelta > 0)) {
						continue;
					}
				}

				long long neighborX = x + xDelta;
				long long neighborY = y + yDelta;
				if (cellExists(neighborX, neighborY))
				{
					Cell *neighbor = board[neighborY][neighborX];
					if (neighbor->alive)
					{
						livingNeighbors++;
					}
				}
				else
				{
					if (checkNewNeighbors && cell->real)
					{
						if (!cellExists(neighborX, neighborY, true))
						{
							Cell *neighbor = new Cell(false, false, false, false);
							checkCell(neighbor, neighborX, neighborY, false);
							newCells[neighborY][neighborX] = neighbor;
						}
					}
				}
			}
		}
		if ((cell->alive && (livingNeighbors < 2 || livingNeighbors > 3)) || (!cell->alive && livingNeighbors == 3))
		{
			cell->willChange = true;
		}
	}
}

// check to see if a cell exists at given coordinates,
// optionally checking in the collection of newly created cells in the middle of a 'tick'
bool Conway2::cellExists(long long x, long long y, bool checkNew)
{
	bool exists = false;
	if (checkNew)
	{
		if (newCells.count(y) > 0)
		{
			if (newCells[y].count(x) > 0)
			{
				exists = true;
			}
		}
	}
	else
	{
		if (board.count(y) > 0)
		{
			if (board[y].count(x) > 0)
			{
				exists = true;
			}
		}
	}
	return exists;
}

// given the state of the board, update the pixel array to be rendered as a sprite texture
void Conway2::updatePixels(sf::Uint8 *pixels, unsigned int width, unsigned int height)
{
	// if at 1:1 or higher magnification
	if (gridPerPixel == 1) {
		for (unsigned int y = 0; y < (height + pixelPerGrid - 1) / pixelPerGrid; y++)
		{
			long long curY = viewY + y;
			std::map<long long, Cell*> curRow = board[curY];
			// if a cell does not exist where the left-most edge of the screen falls, create one
			if (curRow.count(viewX) == 0)
			{
				curRow[viewX] = new Cell(false, false, false, false);
			}
			// fetch an iterator at the cell at the left-most edge of the screen for the current row
			std::map<long long, Cell*>::iterator rowIterator = curRow.find(viewX);
			long long maxX = viewX + (width + pixelPerGrid - 1) / pixelPerGrid;
			// iterate over the row until we examine everything within the screen for that row
			while (rowIterator != curRow.end() && rowIterator->first < maxX)
			{
				if (rowIterator->second->alive)
				{
					// where we have living cells, set any pixels over that cell to white
					for (unsigned int yOffset = 0; yOffset < pixelPerGrid; yOffset++)
					{
						for (unsigned int xOffset = 0; xOffset < pixelPerGrid; xOffset++)
						{
							unsigned int pixelY = (curY - viewY) * pixelPerGrid + yOffset;
							unsigned int pixelX = (rowIterator->first - viewX) * pixelPerGrid + xOffset;
							unsigned int pixelOffset = (pixelY * width * 4) + (pixelX * 4);
							for (unsigned int rgb = 1; rgb < 4; rgb++)
							{
								pixels[pixelOffset + rgb] = 255;
							}
						}
					}
				}
				rowIterator++;
			}
		}
	}
	// lower magnification not implemented
	else
	{

	}
}

// increase or decrease magnification, within limits
void Conway2::zoomIn()
{
	if (pixelPerGrid < 16)
	{
		pixelPerGrid <<= 1;
	}
}
void Conway2::zoomOut()
{
	if (pixelPerGrid > 1)
	{
		pixelPerGrid >>= 1;
	}
}

// manually create a live cell at a location in screen coordinates.
// allows drawing arbitrary starting shapes
void Conway2::cellOn(int x, int y)
{
	long long boardX = viewX + (x / pixelPerGrid);
	long long boardY = viewY + (y / pixelPerGrid);
	if (cellExists(boardX, boardY))
	{
		Cell *cell = board[boardY][boardX];
		cell->alive = true;
		cell->real = true;
	}
	else
	{
		Cell *cell = new Cell(true, true, false, false);
		board[boardY][boardX] = cell;
	}
}

// manually kill a cell at given screen coordinates
void Conway2::cellOff(int x, int y)
{
	long long boardX = viewX + (x / pixelPerGrid);
	long long boardY = viewY + (y / pixelPerGrid);
	if (cellExists(boardX, boardY))
	{
		Cell *cell = board[boardY][boardX];
		cell->alive = false;
	}
}

// move the viewport around by half of the field of view, based on magnification
void Conway2::moveUp(unsigned int height)
{
	viewY -= (height / pixelPerGrid / 2);
}
void Conway2::moveLeft(unsigned int width)
{
	viewX -= (width / pixelPerGrid / 2);
}
void Conway2::moveDown(unsigned int height)
{
	viewY += (height / pixelPerGrid / 2);
}
void Conway2::moveRight(unsigned int width)
{
	viewX += (width / pixelPerGrid / 2);
}

// reset the whole board
void Conway2::reset()
{
	for (std::map<long long, std::map<long long, Cell*>>::iterator it = board.begin(); it != board.end(); ++it)
	{
		for (std::map<long long, Cell*>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
		{
			delete it2->second;
		}
		it->second.clear();
	}
	board.clear();
}
