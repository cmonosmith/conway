#pragma once

//#include "conwayBase.h"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <unordered_map>
#include <exception>

class Conway3 // : public ConwayBase
{
private:
	struct Block
	{
		unsigned long rows[32] = { };
		unsigned long next[32] = { };
		bool active;
		Block *neighbors[8];
		Block(bool active) : active(active), neighbors() { }
	};
	struct PairHasher
	{
		std::size_t operator()(const std::pair<long long, long long>& key) const
		{
			return std::hash<long long>()(key.first) ^ (std::hash<long long>()(key.second) << 1);
		}
	};
	std::unordered_map<std::pair<long long, long long>, Block*, PairHasher> board;
	std::unordered_map<std::pair<long long, long long>, Block*, PairHasher> newBlocks;
	unsigned char bitCount[256];
	sf::Uint8 bytes[256][32];
	sf::Uint8 pixels[32 * 32 * 4]; // * 4 because pixels have 4 components (ARGB)

	unsigned char countBits(unsigned char i);
	void getByte(unsigned char i, sf::Uint8 byte[8]);
	void addBlocksToNeighbors(std::pair<long long, long long> coords, Block *block);
	void makeNewNeighbors(std::pair<long long, long long> coords, Block *block, int neighborIndex, int blockIndex);
	void updateTopRow(Block *block, long long x, long long y);
	void updateTopLeft(Block *block, long long x, long long y);
	void updateTopInner(Block *block, unsigned char bit, long long x, long long y);
	void updateTopRight(Block *block, long long x, long long y);
	void updateCentralRows(Block *block, long long x, long long y, bool includeInner);
	void updateCentralLeft(Block *block, unsigned char row, long long x, long long y);
	void updateCentralInner(Block *block, unsigned char row, unsigned char bit, long long x, long long y);
	void updateCentralRight(Block *block, unsigned char row, long long x, long long y);
	void updateBottomRow(Block *block, long long x, long long y);
	void updateBottomLeft(Block *block, long long x, long long y);
	void updateBottomInner(Block *block, unsigned char bit, long long x, long long y);
	void updateBottomRight(Block *block, long long x, long long y);

public:
	Conway3(std::list<std::pair<long long, long long>> inputs);
	~Conway3();
	void cellActivate(long long x, long long y);
	void cellKill(long long x, long long y);
	void update();
	std::list<std::pair<sf::Sprite *, sf::Texture *>> drawAreaCells(long long x, long long y, unsigned int width, unsigned int height, unsigned int gridPerPixel, unsigned int pixelPerGrid);
	void reset();
};