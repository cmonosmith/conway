#pragma once

#include <SFML/Graphics.hpp>
#include <array>
#include <map>
#include <string>

class Conway2
{
	// curvature == true implies that the universe wraps around naturally with integer overflow
	// if false, the universe has boundaries
	// either way, it's finite
	bool curvature;
	struct Cell
	{
		bool real;
		bool alive;
		bool visited;
		bool willChange;
		//Cell()
		//	: real(false), alive(false), visited(false), willChange(false) {}
		Cell(bool real, bool alive, bool visited, bool willChange)
			: real(real), alive(alive), visited(visited), willChange(willChange) {}
	};

	std::map<long long, std::map<long long, Cell*>> board;
	std::map<long long, std::map<long long, Cell*>> newCells;
	long long viewX;
	long long viewY;
	unsigned char gridPerPixel;
	unsigned char pixelPerGrid;

	void checkCell(Cell *cell, long long x, long long y, bool checkNewNeighbors);
	bool cellExists(long long x, long long y, bool checkNew = false);

public:
	Conway2(std::string inputFile, int curvature);
	~Conway2();
	void update();
	void updatePixels(sf::Uint8*, unsigned int width, unsigned int height);
	void zoomIn();
	void zoomOut();
	void cellOn(int x, int y);
	void cellOff(int x, int y);
	void moveUp(unsigned int height);
	void moveLeft(unsigned int width);
	void moveDown(unsigned int height);
	void moveRight(unsigned int width);
	void reset();
};